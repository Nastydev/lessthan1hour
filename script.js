// // ---------- INPUTS SUBMIT
const submit = document.getElementById('submit')

submit.addEventListener('click', function (event) {

    var error = document.getElementById('error')
    error.innerHTML = ""

    var errorP = document.createElement('p')
    errorP.classList.add('error')

    var forSucess = []
    var tableauError = []

    //USERNAME
    var username = document.getElementById("username").value
    if (!username) {
        tableauError.push("Identifiant obligatoire" + "<br>")
        forSucess = [1]
    } else if (username.length < 8) {
        tableauError.push("Identifiant trop court" + "<br>")
        forSucess = [1]
    }

    // PASSWORD
    var password = document.getElementById("password").value
    if (!password) {
        tableauError.push("Mot de Passe obligatoire" + "<br>")
        forSucess = [1]
    } else if (password.length < 8) {
        tableauError.push("Mot de Passe trop court" + "<br>")
        forSucess = [1]
    } else if (!password.match(/\d+/g)) {
        // } else if (!password.match(/[0-9]/)) {
        tableauError.push("Le mot de passe doit contenir au moins un chiffre" + "<br>")
        forSucess = [1]
    } else if (!password.match(/[*@!#%&()^~{}]+/)) {
        tableauError.push("Le mot de passe doit contenir au moin un caractère spécial" + "<br>")
        forSucess = [1]
    }

    //CONFIRM MOT DE PASS
    var confirmPassword = document.getElementById("confirm-password").value
    if (password !== confirmPassword) {
        tableauError.push("Les mots de passe ne correspondent pas" + "<br>")
        forSucess = [1]
    }

    //MAIL
    var mail = document.getElementById("mail").value
    var mailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    atPos = mail.indexOf("@")
    dotPos = mail.lastIndexOf(".")

    if (!mail) {
        tableauError.push("Le mail est invalide" + "<br>")
        forSucess = [1]
    } else if (!mail.match(mailRegex)) {
        tableauError.push("Le mail est invalide" + "<br>")
        forSucess = [1]
    }
    // else if (mail.includes("@") || mail.includes(".") || atPos > dotPos){
    //         tableauError.push("Le mail est invalide"+ "<br>")
    //         forSucess = [1]
    // }


    //TEL
    var tel = document.getElementById("tel").value
    var numberTel = /^[0-9]{10}$/;
    if (!tel.match(numberTel)) {
        // if (isNaN(numberTel)) {
        tableauError.push("Le téléphone est invalide" + "<br>")
        forSucess = [1]
    }

    if (forSucess.indexOf(1) !== 0) {
        var sucess = document.getElementById('error')
        var sucessP = document.createElement('p')
        sucessP.classList.add('success')
        sucessP.innerHTML = "Le Message à bien été envoyé"
        sucess.appendChild(sucessP)
    } else {
        errorP.innerHTML = tableauError.join("<br>")
        error.appendChild(errorP)
    }


})
